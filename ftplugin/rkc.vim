" Vim filetype plugin
" Language:     RKC
" Maintainer:   Tristan Ninet <ninettristan@gmail.com>
" Last Change:  2023 Feb 21

""" did 

if exists("b:did_ftplugin")
  finish
endif

" set formatlistpat=^\\s*-\\s\\+
let &formatlistpat='^\s*\(\d\+[.)]\?\|-\)\?\s\+'
" Problem : formatting a list with '-' does not correctly indent.

" Soft wrap
setlocal wrap
" Wrap long lines at a character in 'breakat'
setlocal linebreak
" Every wrapped line will continue visually indented.
setlocal breakindent
" Uses the length of a match with formatlistpat for indentation.
setlocal breakindentopt=list:-1

""" Folding 

function! RkcFold()
  let line = getline(v:lnum)

  " Regular headers
  let depth = match(line, '\(^#\+\)\@<=\( .*$\)\@=')
  if depth > 0
    return ">" . depth
  endif

  return "="
endfunction

""" undo

if exists('b:undo_ftplugin')
  let b:undo_ftplugin .= "|setl cms< com< fo< flp<"
else
  let b:undo_ftplugin = "setl cms< com< fo< flp<"
endif

if has("folding")
  setlocal foldexpr=RkcFold()
  setlocal foldmethod=expr
  let b:undo_ftplugin .= " foldexpr< foldmethod<"
endif

