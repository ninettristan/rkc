" Vim syntax file
" Language:     RKC
" Maintainer:   Tristan Ninet <ninettristan@gmail.fr>
" Filenames:    *.rkc
" Last Change:  2022 Apr 20

if exists("b:current_syntax")
  finish
endif

if !exists('main_syntax')
  let main_syntax = 'rkc'
endif

unlet! b:current_syntax

" Makes parsing faster?
syn sync minlines=10
syn case ignore

syn region rkcH1 start="^# "      end="^\s*$"
syn region rkcH2 start="^## "     end="^\s*$"
syn region rkcH3 start="^### "    end="^\s*$"
syn region rkcH4 start="^#### "   end="^\s*$"
syn region rkcH5 start="^##### "  end="^\s*$"
syn region rkcH6 start="^###### " end="^\s*$"

syn match rkcListMarker "^\s*- "

" This causes indented list items not to be highlighted
" syn region rkcCodeBlock start="    \|\t" end="$"

" When running vim in a terminal, vim only uses ctermfg, not guifg.
hi def rkcH1 term=bold ctermfg=1
hi def rkcH2 term=bold ctermfg=2
hi def rkcH3 term=bold ctermfg=3
hi def rkcH4 term=bold ctermfg=4
hi def rkcH5 term=bold ctermfg=5
hi def rkcH6 term=bold ctermfg=6
hi def link rkcListMarker Statement
" hi Folded cterm=bold
" hi Folded ctermfg=0

let b:current_syntax = "rkc"
if main_syntax ==# 'rkc'
  unlet main_syntax
endif

" vim:set sw=2:
