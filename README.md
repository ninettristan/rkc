# Overview

Vim support for my Reformulate Keep Combine (rkc) language.

# The rkc language

The rkc language is simply text with two language constructs of Markdown:
-   multi-level lists with bullet `-`
-   atx-style headers (headers starting with one or more `#`)

The Markdown language aims both to be readable in the source code and to be
converted to HTML.

Unfortunately, this makes the language suboptimal for just taking notes and
viewing them in a text editor.

Indeed in Markdown, URLs should be surrounded by `<` and `>`, code should be
surrounded by \` or indented, and many characters such as `<`, `>` `=`, `*`,
\`, or `_` have special meaning.

Accordingly, text editors supporting the Markdown language perform highlighting
and formatting when encountering these special characters.

If one does not strictly follow the Markdown language, then highlighting and
formatting may appear wrong.

For example, in vim, writing this URL without surrounding it by `<` and `>`
highlights the `_` in red:

<https://example_url.com>

Unlike Markdown, rkc does not seek to be converted to HTML.

rkc only seeks to keep your notes readable and organized.

It does so by having only two language constructs (for readability), which allow
text editors to provide folding and list formatting (for organization).

# This vim plugin

This plugin provides vim support for the rkc language.

Accordingly:
-   List bullets are highlighted
-   Headers are highlighted
-   Folding of headers works
-   Everything else is processed as text

# Installation

Using Vundle:
-   Add `Plugin 'https://gitlab.com/ninettristan/rkc.git'` to your vimrc
-   Source your vimrc and run `:PluginInstall`
